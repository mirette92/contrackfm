import React ,{Component} from 'react';

import {StyleSheet, Text, View,Button,TouchableHighlight,TextInput, Image ,TouchableOpacity, ScrollView} from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';


class ForgetPasswordScreen extends Component{
render(){
  return (
  <View style={styles.container}>

    <View  style={styles.marginTopView}>
    <View style={styles.textStyle}>
      <Text style={styles.colorFont}>Enter your register email address {"\n"} we'll send password reset info on mail</Text>
    </View>
      <View style={styles.paddingMarginInput}>

         <Text style={styles.StyleLabel}>Email Address</Text>

        <View style={styles.IconTextInput}>
         <Ionicons name="ios-mail" color="#e5322d" size={15} style={{marginTop:'2%',}}/>
         <TextInput  placeholderTextColor={'#000'} style={styles.inputStyle}   placeholder="Enter Email Address"/>
       </View>
      </View>


      <TouchableHighlight style={styles.submit}
      onPress={() => this.props.navigation.navigate('Navbar')}
      underlayColor='#fff'>
      <Text style={styles.submitText}>Sign up </Text>
      </TouchableHighlight>

    </View>

</View>
  )
}

}

const styles = StyleSheet.create({
  container: {

  },
  StyleLabel:{
    marginLeft:'8.5%',
    marginBottom:0,
    paddingBottom:0,
    fontSize: 10,
    opacity: 0.3,
  },
  inputStyle:{
    borderBottomColor:'#F7F7F7',
    borderBottomWidth: 1.5,
    width:'90%',
    alignItems:'center',
    marginTop:0,
    paddingTop:0,
    fontSize:12,
  },
  ColorFontApp:{
    color: '#e5322d',
    fontWeight:'bold',
  },
  image: {
    flex:1,
    height: null,
    resizeMode: 'center',
    width: null,
    },
    textStyle: {
      marginTop:'8%',
      alignItems: 'center',
      justifyContent: 'center',
    },
    colorFont:{
      opacity: 0.3,
      paddingLeft:5,
    },
    background:{
      width: '100%',
      height: '40%',
      backgroundColor: '#e5322d',

    },
    marginTopView:{
      marginTop:'1%',
    },
    marginTopInput:{
      height:'3%',
    },
    IconTextInput:{
      flexDirection:'row',
      justifyContent:'space-evenly',
      borderBottomWidth:1,
      borderColor:'#ccc',
    },
    paddingMarginInput:{

      paddingLeft:30,
      marginTop:'20%',

    },
    marginTopBtn:{
      marginTop:'15%',
      width:1,
      borderRadius:10,
      borderWidth: 1,

    },
    submit:{
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor:'#e5322d',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      marginRight:'4%',
      marginLeft:'1%',
      marginTop:20,

  },
  submitText:{
      paddingTop:'3%',
      paddingBottom:'3%',
      textAlign:'center',
      color:'#fff',
    
      width:'90%',
      height:'100%',
      alignItems:'center',
      flexDirection: 'row',
      justifyContent: 'center',
  },
  textSignupStyle:{

    marginTop:'8%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  }
})
export default ForgetPasswordScreen;
