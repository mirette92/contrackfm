import React ,{Component} from 'react';
import { createBottomTabNavigator, BottomTabBar} from 'react-navigation';
import {StyleSheet, Text, View,Button,TouchableHighlight,TextInput, Image ,TouchableOpacity, ScrollView} from 'react-native';
import HomeScreen from './MoreScreen';
import ProfileScreen from './ProfileScreen';
import ChatScreen from './ChatScreen';
import OrdersScreen from './OrdersScreen';
import MoreScreen from './MoreScreen';
import EmployeesScreen from './EmployeesScreen'

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';


class NavbarScreen extends React.Component {
  static navigationOptions = {
    header: {
      visible: false,
    }
  }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ProfileScreen/>
        <MoreScreen/>
        <ChatScreen/>
        <OrdersScreen/>
        <EmployeesScreen/>
      </View>
    );
  }
}

const TabScreens = createBottomTabNavigator(
  {
    Orders:  {
        screen:OrdersScreen,
        navigationOptions: () => ({
                tabBarIcon: ({tintColor}) => (
                    <FontAwesome
                        name="calendar"
                        color={tintColor}
                        size={24}
                    />
                )
            })
      },

      Employees:{
        screen:EmployeesScreen,
        navigationOptions: () => ({

          tabBarVisible: true,
          headerStyle: {
            backgroundColor: '#e5322d'
          },
          tabBarIcon: ({tintColor}) => (
              <FontAwesome
                  name="users"
                  color={tintColor}
                  size={24}
              />
          )

          })

      },
      Profile: {
        screen:ProfileScreen,
        navigationOptions: () => ({
          header: null,
          tabBarVisible: true,
          headerMode: "screen",
          tabBarIcon: ({tintColor}) => (
              <FontAwesome
                  name="user"
                  color={tintColor}
                  size={24}
              />
          )
          })
      },
      Chat: {
        screen:ChatScreen,
        navigationOptions: () => ({
                tabBarIcon: ({tintColor}) => (
                    <Entypo
                        name="chat"
                        color={tintColor}
                        size={24}
                    />
                )
          })
      },
      More: {
        screen:MoreScreen,
        navigationOptions: () => ({
          header: null,
          tabBarVisible: true,
          headerMode: "screen",
          tabBarIcon: ({tintColor}) => (
              <Ionicons
                  name="md-more"
                  color={tintColor}
                  size={24}
              />
          )
          })
      },


    },
    {
      tabBarOptions: {
        activeTintColor: '#e5322d',

      }
  },
);




export default createBottomTabNavigator(TabScreens);
