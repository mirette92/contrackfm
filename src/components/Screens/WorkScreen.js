import React, {Component} from 'react';

import {StyleSheet, View,TouchableHighlight,TextInput, Image ,TouchableOpacity, ScrollView,ImageBackground} from 'react-native';
import { Container, Header, Content, List, ListItem, Text, Icon, Right,Left,Button,Body,Title,CardItem,Card} from 'native-base';
import { SearchBar } from 'react-native-elements';

import { createStackNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';

import Feather from 'react-native-vector-icons/Feather'


class WorkScreen extends React.Component {
  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };
  render() {
    const { search } = this.state;
    return (
      <View style={styles.container}>

      <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d'}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <Ionicons size={20} style={{color:'#fff'}} name="ios-arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title></Title>
        </Body>
        <Right>
          <Button transparent onPress={() => this.props.navigation.navigate('Notification')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications-outline" />
          </Button>
          <Button transparent onPress={() => this.props.navigation.navigate('Notification')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
          </Button>
        </Right>
      </Header>

      <View style={styles.background}>
        <Text style={{fontSize:25,color:'#fff',marginTop:'10%',marginBottom:'20%',fontWeight:'bold'}}>  Service Name </Text>
      </View>
      <View style={styles.centerContent}>
        <View style={styles.inputContainer}>
          <Ionicons name="md-search" size={20} style={[styles.paddingLeft2]}/>
           <TextInput style={[styles.inputs,styles.paddingLeft4]}
           placeholder="Search" placeholderTextColor="#E2E2E2"
           underlineColorAndroid='transparent'
           />
        </View>
      </View>
        <Container>
        <View style={styles.container}>


         <Content  style={{backgroundColor:'#E2E2E2'}}>

           <List style={{flex:1,height:'100%'}}>
             <ListItem noIndent style={[styles.backgroundListItemColor]} onPress={() => this.props.navigation.navigate('AddOrder')}>
               <Left style={styles.marginTop}>
                 <Text style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right style={styles.marginTop}>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor} onPress={() => this.props.navigation.navigate('AddOrder')}>
              <Left>
                 <Text  style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor} onPress={() => this.props.navigation.navigate('AddOrder')}>
               <Left>
                 <Text style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor} onPress={() => this.props.navigation.navigate('AddOrder')}>
               <Left>
                 <Text style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor} onPress={() => this.props.navigation.navigate('AddOrder')}>
               <Left>
                 <Text style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor} onPress={() => this.props.navigation.navigate('AddOrder')}>
               <Left>
                 <Text style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor} onPress={() => this.props.navigation.navigate('AddOrder')}>
               <Left>
                 <Text style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor} onPress={() => this.props.navigation.navigate('AddOrder')}>
               <Left>
                 <Text style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor} onPress={() => this.props.navigation.navigate('AddOrder')}>
               <Left>
                 <Text style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor} onPress={() => this.props.navigation.navigate('AddOrder')}>
               <Left>
                 <Text style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor} onPress={() => this.props.navigation.navigate('AddOrder')}>
               <Left>
                 <Text style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor} onPress={() => this.props.navigation.navigate('AddOrder')}>
               <Left>
                 <Text style={styles.textListItem}>Service Name</Text>
               </Left>
               <Right>
                 <Ionicons style={styles.iconListItem} name="ios-arrow-forward" />
               </Right>
             </ListItem>

           </List>
         </Content>
         </View>

         <TouchableOpacity
         onPress={() => this.props.navigation.navigate('AddOrder')}
           style={{
               borderWidth:1,
               borderColor:'#e5322d',
               alignItems:'center',
               justifyContent:'center',
               width:'40%',
               position: 'absolute',
               bottom: 10,
               right:'30%',
               height:40,
               backgroundColor:'#e5322d',
               borderRadius:20,
             }}
           >
           <Text style={{color:'#fff'}}>Skip</Text>
          </TouchableOpacity>
         </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  textListItem:{
    fontWeight:'bold'
  },
  iconListItem:{
    fontWeight:'bold'
  },
  textCard:{
    marginLeft:'5%'
  },
  whiteBackground:{
    backgroundColor:'#00000000',
    borderBottomWidth:0,
    borderTopWidth:0,
  },
  marginTop:{
    marginTop:'10%'
  },
  inputSearchStyle:{
      backgroundColor:'#E2E2E2',

  },
  container: {
    display: 'flex',
    flex:1,
  },
  TextStyle: {
    marginLeft:'3%'
  },
  TextStyleVersion: {
    textAlign:'center',
  },
  backgroundListItemColor:{

    backgroundColor:'#fff',
    borderBottomWidth:3,
    borderColor:'#E2E2E2'
  },
  image: {
    height: null,
    width: '100%',

    },
    textStyle: {
      marginTop:'8%',
      alignItems: 'center',
      justifyContent: 'center',
    },

    background:{
      height: null,
      width: '100%',
      backgroundColor: '#e5322d',
      alignItems:'center',
      justifyContent:'center',

    },

    inputContainer: {

      flexDirection: 'row',
      alignItems:'center',
      borderColor:'#E2E2E2',

      borderWidth: 2,
      position: 'absolute',
      zIndex: 1,
      backgroundColor: '#fff',
      width:'90%'
   },
   inputs:{
     backgroundColor:'transparent',
     fontSize:20,

  },
  paddingLeft2:{
    paddingLeft:'2%'
  },
  paddingLeft4:{
    paddingLeft:'4%'
  },
  centerContent:{
    marginTop:'2%',
    alignItems:'center',
    justifyContent:'center'
  }

})
export default WorkScreen;
