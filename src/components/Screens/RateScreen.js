//imports
import React, {Component} from 'react';
import {StyleSheet, View,TouchableHighlight,TextInput, Image ,TouchableOpacity,Text } from 'react-native';

import { Container, Header, Content, Tab, Tabs,Textarea,Item } from 'native-base';
import { createMaterialTopTabNavigator, createTabNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Rating, AirbnbRating } from 'react-native-elements';

import Ionicons from 'react-native-vector-icons/Ionicons';

//body(jsx)
class RateScreen extends Component{



  render(){
    return (
    <View style={styles.container}>
          <View style={styles.viewAliginCenter}><Text style={styles.ColorFontApp}>Please rate your {"\n"}       Techncian {"\n"} for a better Service</Text></View>
          <View  style={styles.marginTopView}>
            <View style={styles.paddingMarginRow}>
              <FontAwesome name="user" color="#e5322d" size={50} style={{paddingRight:'5%'}} />
              <View size={50} style={{paddingTop:'5%'}}>
              <Text style={styles.techNameStyle}>Ahmed Mohamed | techncian.</Text><Text style={styles.techPostionStyle}>4.6 <FontAwesome name="star" color="#F0C30D" size={15}/></Text>
              </View>
            </View>
            <View style={styles.paddingMarginRow}>

               <AirbnbRating reviews={[]} />
            </View>
            <View style={styles.paddingMarginRow}><Text style={styles.ColorSizeFontApp}>if the job was not fullfill please {"\n"}               Press Here</Text></View>
            <View style={styles.paddingMarginTextarea}>

                <Textarea rowSpan={5}  placeholder="Textarea" />
               </View>
            </View>
            <TouchableHighlight style={styles.submit}
            onPress={() => this.props.navigation.navigate('Navbar')}
            underlayColor='#fff'>
            <Text style={styles.submitText}>Submit </Text>
            </TouchableHighlight>
      </View>
    );

  }
}


class SignupScreenCoperate extends Component{
  render(){
    return (
      <View>
        <Text>Hello</Text>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  techPostionStyle:{
    fontSize:12,
    opacity:0.5,
  },
  techNameStyle:{
    fontWeight:'bold',
    fontSize:13
  },
  IconTextInput:{
    flexDirection:'row',
    justifyContent:'space-evenly',
    borderBottomWidth:1,
    borderColor:'#ccc',
  },
  container: {
    marginTop:'3%',

    flexDirection: 'row',
    justifyContent:'space-evenly',

    flexWrap: 'wrap' ,

  },


  viewAliginCenter:{
    justifyContent:'space-evenly',
  },

  ColorFontApp:{
    color: '#e5322d',

    fontSize:20,
  },
  ColorSizeFontApp:{
    color: '#e5322d',

    fontSize:15,
  },


    colorFont:{
      opacity: 0.3,
      paddingLeft:5,
    },
    background:{
      width: '100%',
      height: '40%',
      backgroundColor: '#e5322d',

    },
    marginTopView:{
      marginTop:'20%',
    },
    marginTopInput:{
      height:'3%',
    },
    paddingMarginInput:{

      marginTop:5,

    },
    paddingMarginTextarea:{
      paddingLeft:7,
      marginTop:20,
      borderRadius:10,
      borderWidth: 1,
      borderColor:'#E2E2E2',
      backgroundColor:'#E2E2E2',
    },
    paddingMarginRow:{
      marginTop:5,
      flexDirection:'row',
      justifyContent:'center'
    },
    marginTopBtn:{
      marginTop:'10%',
      width:1,
      borderRadius:10,
      borderWidth: 1,

    },

    submit:{
      flexDirection: 'row',
      justifyContent: 'center',
      marginRight:'4%',
      marginLeft:'1%',
      marginTop:20,

  },

  TextStyle: {
    marginLeft:'20%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0
  },

  submitText:{
      paddingTop:'3%',
      paddingBottom:'3%',
      textAlign:'center',
      color:'#fff',
      backgroundColor:'#e5322d',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      width:'90%',
      height:'100%',
      alignItems:'center',
      flexDirection: 'row',
      justifyContent: 'center',
  },


});




export default RateScreen;

//export
