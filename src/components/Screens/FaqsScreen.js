import React, {Component} from 'react';


import { Container, Header, Content, List, ListItem, Text, Icon, Right,Left,Body,Title,Button} from 'native-base';
import { createStackNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';

import Feather from 'react-native-vector-icons/Feather'
import {StyleSheet} from 'react-native';
import MyStatusBarCustom from '../../helpers/MyStatusBar';

class FaqsScreen extends React.Component {
  render() {
    return (
      <Container >
      <MyStatusBarCustom backgroundColor = "#952421"/> 
      <Header iosBarStyle="dark-content" noShadow style={{backgroundColor:'#e5322d'}}>
        
        <Left style={{justifyContent:'center',textAlign:'center'}}>
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back"  />
          </Button>
        </Left>
        <Body>
          <Title>FAQs & Terms</Title>
        </Body>
        <Right>
        <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
          <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications-outline" />
        </Button>
        <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
          <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
        </Button>
        </Right>
      </Header>
         <Content style={{backgroundColor:'#E2E2E2'}}>
           <List>
             <ListItem noIndent style={styles.backgroundListItemColor}>
               <Left>
                 <Text>About Services</Text>
               </Left>
               <Right>
                 <Ionicons name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor}>
              <Left>
                 <Text>Sign in & Sign up</Text>
               </Left>
               <Right>
                 <Ionicons name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor}>
               <Left>
                 <Text>Payment Policy</Text>
               </Left>
               <Right>
                 <Ionicons name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor}>
               <Left>
                 <Text>Searching Service</Text>
               </Left>
               <Right>
                 <Ionicons name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor}>
               <Left>
                 <Text>Ratings</Text>
               </Left>
               <Right>
                 <Ionicons name="ios-arrow-forward" />
               </Right>
             </ListItem>
             <ListItem noIndent style={styles.backgroundListItemColor}>
               <Left>
                 <Text>chatting</Text>
               </Left>
               <Right>
                 <Ionicons name="ios-arrow-forward" />
               </Right>
             </ListItem>
           </List>
         </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  TextStyle: {
    marginLeft:'3%'
  },
  TextStyleVersion: {
    textAlign:'center',
  },
  backgroundListItemColor:{

    backgroundColor:'#fff',
  }
})
export default FaqsScreen;
