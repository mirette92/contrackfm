//imports
import React ,{Component} from 'react';

import {StyleSheet,View,TouchableHighlight, ImageBackground,TextInput, Image ,TouchableOpacity, ScrollView} from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { Container, Header, Content, List, ListItem, Text, Icon, Right,Left,Body,Title,Button,Tab,Tabs,Segment,Card, CardItem} from 'native-base';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {ButtonGroup} from 'react-native-elements'
import Services from './Services'
import Completed from './Completed'
import Current from './Current'
//body(class-[functions-methodes-jsx])

class OrderScreen extends Component{
  constructor () {
    super()
    this.state = {
      activePage: 1
    }

  }
  selectComponent = (activePage) => () => this.setState({activePage})

  _renderComponent = () => {
    if(this.state.activePage === 1)
      return <Services/> //... Your Component 1 to display
    else if(this.state.activePage === 2)
      return <Current/>
    else
     return <Completed/> //... Your Component 2 to display
  }



  render(){

    return(
    <View style={styles.container}>
    
      <Header iosBarStyle="dark-content" androidStatusBarColor="#952421" style={{display:'none'}} />

      <ImageBackground style={styles.image} source={{uri:'https://www.contrackfm.com/wp-content/uploads/2018/02/home-background.jpg'}}>

          <Button transparent style={{position: 'absolute',right: 50}} onPress={() => this.props.navigation.navigate('Notification')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications-outline" />
          </Button>
          <Button transparent style={{position: 'absolute', right: 10}} onPress={() => this.props.navigation.navigate('Chat')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
          </Button>

        <Text style={{fontSize:25,color:'#fff',marginTop:'10%',marginBottom:'20%',fontWeight:'bold'}}>  How can we {"\n"} {" "}Help you today? </Text>
      </ImageBackground>
      <View style={{width:'100%',flexWrap:'wrap'}}>
        <View style={{backgroundColor:'#fff',flexDirection:'row',justifyContent:'space-between'}}>
          <Button transparent light style={styles.segmentbtn} first active={this.state.activePage === 1} onPress={this.selectComponent(1)}>
            <Ionicons size={20} style={{color:'#252525',paddingLeft:'2%'}} name="ios-add" />
            <Text style={styles.textSegmentbtn}>New Order</Text>
          </Button>
          <Button transparent light style={styles.segmentbtn} active={this.state.activePage === 2} onPress={this.selectComponent(2)}>
            <MaterialCommunityIcons size={20} style={{color:'#252525',paddingLeft:'2%'}} name="timer-sand" />
            <Text style={styles.textSegmentbtn}>Current</Text>
          </Button>
          <Button transparent light style={styles.segmentbtn} last active={this.state.activePage === 3} onPress={this.selectComponent(3)}>
            <Ionicons size={20} style={{color:'#252525',paddingLeft:'1%'}} name="ios-timer" />
            <Text style={styles.textSegmentbtn}>Completed</Text>
          </Button>
        </View>
      </View>
      <View style={styles.container}>
      {this._renderComponent()}
      </View>
    </View>

    )
  }
}


//css (styling)
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex:1
  },
  textSegmentbtn:{
      color:'#252525',
      fontSize:14,
      paddingLeft:0,
  },
    segmentbtn:{
    flexGrow:1,
    borderWidth:1,
    borderTopWidth:3,
    borderColor:'#c2c2c2',
    borderRadius:0
  },
  StyleLabel:{
    marginLeft:'8.5%',
    marginBottom:0,
    paddingBottom:0,
    fontSize: 10,
    opacity: 0.3,
  },
  inputStyle:{
    borderBottomColor:'#F7F7F7',
    borderBottomWidth: 1.5,
    width:'90%',
    alignItems:'center',
    marginTop:0,
    paddingTop:0,
    fontSize:12,
  },
  ColorFontApp:{
    color: '#e5322d',
    fontWeight:'bold',
  },
  image: {
    height: null,
    width: '100%',

    },
    textStyle: {
      marginTop:'8%',
      alignItems: 'center',
      justifyContent: 'center',
    },
    colorFont:{
      opacity: 0.3,
      paddingLeft:5,
    },
    background:{
      width: '100%',
      height: '40%',
      backgroundColor: '#e5322d',

    },
    ImgTextInput:{
      marginTop:'1%',
      flexDirection:'row',
      justifyContent:'space-evenly',
    },
    marginTopInput:{
      height:'3%',
    },
    IconTextInput:{
      flexDirection:'row',
      justifyContent:'space-evenly',
      borderBottomWidth:1,
      borderColor:'#ccc',
    },
    paddingMarginInput:{

      paddingLeft:30,
      marginTop:10,

    },
    marginTopBtn:{
      marginTop:'10%',
      width:1,
      borderRadius:10,
      borderWidth: 1,

    },
    submit:{
      flexDirection: 'row',
      justifyContent: 'center',
      marginRight:'4%',
      marginLeft:'1%',
      marginTop:20,

  },
  submitText:{
      paddingTop:'3%',
      paddingBottom:'3%',
      textAlign:'center',
      color:'#fff',
      backgroundColor:'#e5322d',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      width:'90%',
      height:'100%',
      alignItems:'center',
      flexDirection: 'row',
      justifyContent: 'center',
  },
  textSignupStyle:{

    marginTop:'8%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  }

});


//exports
export default OrderScreen;
