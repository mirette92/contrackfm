import React, {Component} from 'react';


import { Container, Header, Content, List, ListItem, Text, Icon, Right,Body,Title,Left,Button} from 'native-base';
import { createStackNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';

import Feather from 'react-native-vector-icons/Feather'
import {StyleSheet} from 'react-native';

class MoreScreen extends React.Component {
  render() {
    return (

      <Container >
        <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d'}}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack(null)}>
              <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>More</Title>
          </Body>
          <Right>
            <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
              <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications-outline" />
            </Button>
            <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
              <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
            </Button>
          </Right>
        </Header>
        <Content style={{backgroundColor:'#E2E2E2'}}>
          <List>
            <ListItem noIndent onPress={() => this.props.navigation.navigate('Offers')} style={{backgroundColor:'#fff'}} style={{backgroundColor:'#fff'}}>
              <Feather name="book-open" color="#e5322d" size={18} />
              <Text style={styles.TextStyle}>Offer</Text>

            </ListItem>
            <ListItem noIndent  style={{backgroundColor:'#fff'}}>
              <Ionicons name="ios-mail" color="#e5322d" size={18} />
              <Text style={styles.TextStyle}>Contact Us</Text>

            </ListItem>
            <ListItem noIndent  style={{backgroundColor:'#fff'}}>
              <FontAwesome name="lock" color="#e5322d" size={18} />
              <Text style={styles.TextStyle}>Privacy Policy</Text>

            </ListItem>
            <ListItem noIndent onPress={() => this.props.navigation.navigate('About')} style={{backgroundColor:'#fff'}}>
              <Ionicons name="md-contact" color="#e5322d" size={18} />
              <Text style={styles.TextStyle}>About Us</Text>

            </ListItem>
            <ListItem noIndent onPress={() => this.props.navigation.navigate('Faq')} style={{backgroundColor:'#fff'}}  style={{backgroundColor:'#fff'}}>
              <FontAwesome name="list" color="#e5322d" size={18} />
              <Text style={styles.TextStyle}>FAQs & terms</Text>

            </ListItem>
            <ListItem noIndent  style={{backgroundColor:'#fff'}}>
              <FontAwesome name="power-off" color="#e5322d" size={18} />
              <Text style={styles.TextStyle}>Signout</Text>

            </ListItem>
            <ListItem noIndent  style={{backgroundColor:'#fff'}}>

              <Text style={styles.TextStyleVersion}>V 1.0.1</Text>

            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  TextStyle: {
    marginLeft:'3%'
  },
  TextStyleVersion: {
    textAlign:'center',
  },
})
export default MoreScreen;
