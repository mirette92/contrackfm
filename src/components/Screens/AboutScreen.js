//imports
import React ,{Component} from 'react';

import {StyleSheet, View,TouchableHighlight,TextInput, Image ,TouchableOpacity, ScrollView} from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { Container, Header, Content, List, ListItem, Text, Icon, Right,Left,Body,Title,Button} from 'native-base';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';


//body(class-[functions-methodes-jsx])
class AboutScreen extends Component{
  onPressLogin = (event)=>{
    alert('you logged successfully');
  }

  render(){
    return(
    <View style={styles.container}>
      <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d'}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.goBack(null)}>
            <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>About</Title>
        </Body>
        <Right>
          <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications-outline" />
          </Button>
          <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
          </Button>
        </Right>
      </Header>
      <View style={styles.background}>
        <Image style={styles.image} source={require('./icon1.png')}/>
      </View>
      <View  style={styles.marginTopView}>
      <Text style={styles.StyleLabel}>About</Text>
      <View>
      <Text>Pests are not going to be a problem anymore with CFM’s services. Pest control services are provided under strict controls pertaining to the health and safety of the building’s occupants, and limiting the impact on the environmental, as much as possible. All our chemicals are environmentally friendly and affect neither the surface nor the inhabitants.</Text></View>
      </View>
    </View>
    )
  }
}


//css (styling)
const styles = StyleSheet.create({
  container: {

  },
  StyleLabel:{
    color: '#e5322d',
    fontWeight:'bold',
    fontSize: 15,
  },
  inputStyle:{
    borderBottomColor:'#F7F7F7',
    borderBottomWidth: 1.5,
    width:'90%',
    alignItems:'center',
    marginTop:0,
    paddingTop:0,
    fontSize:12,
  },
  ColorFontApp:{
    color: '#e5322d',
    fontWeight:'bold',
  },
  image: {
    flex:1,
    height: null,
    resizeMode: 'center',
    width: null,
    },
    textStyle: {
      marginTop:'8%',
      alignItems: 'center',
      justifyContent: 'center',
    },
    colorFont:{
      opacity: 0.3,
      paddingLeft:5,
    },
    background:{
      width: '100%',
      height: '40%',
      backgroundColor: '#e5322d',

    },
    marginTopView:{

      marginTop:'1%',
      marginLeft:'3%',
      marginRight:'4%',
    },
    marginTopInput:{
      height:'3%',
    },
    IconTextInput:{
      flexDirection:'row',
      justifyContent:'space-evenly',
      borderBottomWidth:1,
      borderColor:'#ccc',
    },
    paddingMarginInput:{

      paddingLeft:30,
      marginTop:10,

    },
    marginTopBtn:{
      marginTop:'10%',
      width:1,
      borderRadius:10,
      borderWidth: 1,

    },
    submit:{
      flexDirection: 'row',
      justifyContent: 'center',
      marginRight:'4%',
      marginLeft:'1%',
      marginTop:20,

  },
  submitText:{
      paddingTop:'3%',
      paddingBottom:'3%',
      textAlign:'center',
      color:'#fff',
      backgroundColor:'#e5322d',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      width:'90%',
      height:'100%',
      alignItems:'center',
      flexDirection: 'row',
      justifyContent: 'center',
  },
  textSignupStyle:{

    marginTop:'8%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  }

});


//exports
export default AboutScreen;
