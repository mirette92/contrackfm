import React, {Component} from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { Container, Header, Content, List, ListItem,  Icon, Right,Body,Title,Left,Button} from 'native-base';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  TextInput,
  FlatList
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

class ChatScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {id:1, date:"9:50 am", type:'in',  message: "Lorem ipsum dolor sit amet"},
        {id:2, date:"9:50 am", type:'out', message: "Lorem ipsum dolor sit amet"} ,
        {id:3, date:"9:50 am", type:'in',  message: "Lorem ipsum dolor sit a met"},
        {id:4, date:"9:50 am", type:'in',  message: "Lorem ipsum dolor sit a met"},
        {id:5, date:"9:50 am", type:'out', message: "Lorem ipsum dolor sit a met"},
        {id:6, date:"9:50 am", type:'out', message: "Lorem ipsum dolor sit a met"},
        {id:7, date:"9:50 am", type:'in',  message: "Lorem ipsum dolor sit a met"},
        {id:8, date:"9:50 am", type:'in',  message: "Lorem ipsum dolor sit a met"},
        {id:9, date:"9:50 am", type:'in',  message: "Lorem ipsum dolor sit a met"},
      ]
    };
  }
  renderDate = (date) => {
   return(
     <Text style={styles.time}>
       {date}
     </Text>
   );
 }
  render() {
    return (
      <View style={styles.container}>
      <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d'}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.goBack(null)}>
            <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>Contrack FM</Title>
        </Body>
        <Right>
        <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
          <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications-outline" />
        </Button>
        <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
          <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
        </Button>
        </Right>
      </Header>
        <FlatList style={styles.list}
          data={this.state.data}
          keyExtractor= {(item) =>
             item.id.toString()
          }
          renderItem={(message) => {
            console.log(item);
            const item = message.item;
            let inMessage = item.type === 'in';
            let itemStyle = inMessage ? styles.itemIn : styles.itemOut;
            let itemColor = inMessage ? styles.itemRed :styles.itemWhite;
            return (
              <View style={[itemColor, itemStyle]}>
                {!inMessage && this.renderDate(item.date)}
                <View style={[styles.balloon]}>
                  <Text>{item.message}</Text>
                </View>
                {inMessage && this.renderDate(item.date)}
              </View>
            )
          }}/>
        <View style={styles.footer}>
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Write a message..."
                underlineColorAndroid='transparent'
                onChangeText={(name_address) => this.setState({name_address})}/>
          </View>

            <TouchableOpacity style={styles.btnSend}>
              <Image source={{uri:"https://png.icons8.com/small/75/ffffff/filled-sent.png"}} style={styles.iconSend}  />
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#E2E2E2',
  },
  list:{
    paddingHorizontal: 17,
  },
  footer:{
    flexDirection: 'row',
    height:60,
    backgroundColor: '#eeeeee',
    paddingHorizontal:10,
    padding:5,
  },
  btnSend:{
    backgroundColor:"#e5322d",
    width:40,
    height:40,
    borderRadius:360,
    alignItems:'center',
    justifyContent:'center',
  },
  iconSend:{
    width:30,
    height:30,
    alignSelf:'center',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    borderBottomWidth: 1,
    height:40,
    flexDirection: 'row',
    alignItems:'center',
    flex:1,
    marginRight:10,
  },
  inputs:{
    height:40,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
  },
  balloon: {
    maxWidth: 250,
    padding: 15,
    borderRadius: 20,
  },
  itemIn: {
    alignSelf: 'flex-start'
  },
  itemOut: {
    alignSelf: 'flex-end'
  },
  time: {
    alignSelf: 'flex-end',
    margin: 15,
    fontSize:12,
    color:"#808080",
  },
  itemWhite: {
    marginVertical: 14,
    flex: 1,
    flexDirection: 'row',
    backgroundColor:"#fff",
    borderRadius:300,
    padding:5,
  },
  itemRed: {
    marginVertical: 14,
    flex: 1,
    flexDirection: 'row',
    backgroundColor:"#e5322d",
    borderRadius:300,
    padding:5,
  },
});
export default ChatScreen;
