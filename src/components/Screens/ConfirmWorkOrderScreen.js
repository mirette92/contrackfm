import React, {Component} from 'react';
import {Platform, StyleSheet,  View,Image,ImageBackground,TouchableHighlight} from 'react-native';
import { Container, Header, Content, Card, CardItem, Body,Left,Right,Title,Button,Textarea } from "native-base";
import { createStackNavigator, createAppContainer } from "react-navigation";

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Avatar,Text } from 'react-native-elements';



class ConfirmWorkOrderScreen extends React.Component {

  onPressLogin = (event)=>{
    alert('you logged successfully');
  }
  render() {
    return (
      <Container>
      <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d'}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.goBack(null)}>
            <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>Confirm Work Order</Title>
        </Body>
        <Right>
          <Button transparent >

          </Button>
        </Right>
      </Header>
        <Content style={{backgroundColor:'#E2E2E2'}}>

          <Card style={{backgroundColor:'#fff'}}>
          <CardItem>
            <Body >
              <View style={styles.CardProfile}>
              <Avatar
              size="large"
              rounded
              source={{
                uri:
                  'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                }}
                />
                <View style={{flexFlow:'column'||'wrap',paddingTop:'6%'}}>
                  <Text style={styles.companyTitle}>
                    Vodafone
                  </Text>
                  <Text style={styles.subTitle}>
                    250 Work Order
                  </Text>
                  <Text style={styles.title}>
                  Job Task
                  </Text>
                  <Text style={styles.answers}>
                    Tap ,wash basee in and sink problem
                  </Text>
                  <Text style={styles.title}>
                    Address
                  </Text>
                  <Text style={styles.answers}>
                    NileCity building , cairo ,egypt
                  </Text>
                </View>
              </View>
            </Body>
          </CardItem>
        </Card>
        <Text style={styles.subTitle}>
          Signture Authorization
        </Text>
        <View style={styles.centerContent}>
          <View style={styles.paddingMarginTextarea}>
              <Textarea rowSpan={5}  placeholder="Textarea" />
          </View>
          <TouchableHighlight style={styles.submit}
          onPress={() => this.onPressLogin()}
          underlayColor='#fff'>
          <Text style={styles.submitText}>Confirm</Text>
          </TouchableHighlight>
        </View>

        </Content>

      </Container>

    );
  }

}

const styles = StyleSheet.create({
  CardProfile: {
    flexDirection:'row',

  },
  centerContent:{
    justifyContent: 'center',
    alignItems:'center'
  },
  paddingMarginTextarea:{
    width:'70%',

    paddingLeft:7,
    marginTop:10,
    borderRadius:10,
    borderWidth: 1,
    borderColor:'#E2E2E2',
    backgroundColor:'#fff',

  },
  companyTitle:{
    fontSize:15,paddingLeft:'3%',color:'#252525',fontWeight:'600'
  },
  subTitle:{
      fontSize:15,paddingLeft:'3%',color:'#c2c2c2'
  },
  title:{
    fontSize:12,color:'#c2c2c2',paddingTop:'5%'
  },
  answer:{
    fontSize:12,paddingLeft:'4%',color:'#252525'
  },
  TextStyleVersion: {
    textAlign:'center',
  },
  image: {

    resizeMode:'contain'
    },
    cardSign:{
      backgroundColor:'#fff',

    },
    submit:{
      flexDirection: 'row',
      justifyContent: 'center',
      marginRight:'4%',
      marginLeft:'1%',
      marginTop:20,

  },
  submitText:{
      paddingTop:'3%',
      paddingBottom:'3%',
      textAlign:'center',
      color:'#fff',
      backgroundColor:'#e5322d',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      width:'90%',
      height:'100%',
      alignItems:'center',
      flexDirection: 'row',
      justifyContent: 'center',
  },

})
export default ConfirmWorkOrderScreen;
