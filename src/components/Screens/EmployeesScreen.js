import React, {Component} from 'react';


import { Container, Header, Content, List,  Text, Icon, Right,Left,Button,Title,Body,Accordion} from 'native-base';
import { createStackNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Feather from 'react-native-vector-icons/Feather'
import {StyleSheet,View} from 'react-native';
import { ListItem } from 'react-native-elements'
import { withNavigation } from 'react-navigation';

const dataArray = [
  { title: "Employee 2",position:"Position",workOrder:"No current work order" ,content: "Lorem ipsum dolor sit amet" },
  { title: "Employee 2",position:"Position",workOrder:"No current work order" ,content: "Lorem ipsum dolor sit amet" },
  { title: "Employee 2",position:"Position",workOrder:"No current work order" ,content: "Lorem ipsum dolor sit amet" },
  { title: "Employee 2",position:"Position",workOrder:"No current work order" ,content: "Lorem ipsum dolor sit amet" },
  { title: "Employee 2",position:"Position",workOrder:"No current work order" ,content: "Lorem ipsum dolor sit amet" },
  { title: "Employee 2",position:"Position",workOrder:"No current work order" ,content: "Lorem ipsum dolor sit amet" },
  { title: "Employee 2",position:"Position",workOrder:"No current work order" ,content: "Lorem ipsum dolor sit amet" },
  { title: "Employee 2",position:"Position",workOrder:"No current work order" ,content: "Lorem ipsum dolor sit amet" },

];
class EmployeesScreen extends React.Component {
  _renderHeader(item, expanded) {
    return (
      <View style={{
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-between",
        alignItems: "center" ,
        backgroundColor: "#fff" }}>
          <View style={{
            flexDirection: "row",justifyContent: "space-between",}}>
            <FontAwesome name="user" color="#e5322d" size={40} />
            <Text style={styles.TextStyle}>{" "}{item.title} |{item.position}.{"\n"}{item.workOrder}</Text>
          </View>
        {expanded
          ? <Icon style={{ fontSize: 18 }} name="remove" />
          : <Icon style={{ fontSize: 18 }} name="add" />}
      </View>
    );
  }
  _renderContent(item) {
    return (
      <View >
      <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>
          <Button transparent onPress={() => this.props.navigation.navigate('Employees')}>
            <MaterialIcons name="done" color="green" size={20} />
            <Text style={{color:'green'}}>Accept</Text>
          </Button>
          <Button transparent  onPress={() => this.props.navigation.navigate('Employees')}>
            <Ionicons name="ios-close" color="#e5322d" size={20} />
            <Text style={{color:'#e5322d'}}>reject</Text>
          </Button>

      </View>
      <Text
        style={{
          backgroundColor: "#E2E2E2",
          padding: 10,

          fontSize:14
        }}
      >
        Your WorkOrder :{item.content}
      </Text>
      <Text
        style={{
          backgroundColor: "#E2E2E2",
          padding: 10,
          fontSize:14
        }}
      >
          Status :{item.content}
      </Text>
      <Text
        style={{
          backgroundColor: "#E2E2E2",
          padding: 10,
          fontSize:14
        }}
      >
        Job Task :{item.content}
      </Text>
      <Text
        style={{
          backgroundColor: "#E2E2E2",
          padding: 10,
          fontSize:14
        }}
      >
        {item.content}
      </Text>
      <Text
        style={{
          backgroundColor: "#E2E2E2",
          padding: 10,
          fontSize:14
        }}
      >
        Addres: {item.content}
      </Text>
      </View>
    );
  }
  render() {
    return (
      <Container >
        <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d'}}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack(null)}>
              <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Employees</Title>
          </Body>
          <Right>
          <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications-outline" />
          </Button>
          <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
          </Button>
          </Right>
        </Header>
        <Content style={{backgroundColor:'#E2E2E2'}}>
        <Accordion dataArray={dataArray} renderHeader={this._renderHeader}
            renderContent={this._renderContent} expanded={0}/>


        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  TextStyle: {
    marginLeft:'2%',
  },
  TextStyleVersion: {
    textAlign:'center',
  },
})
export default  withNavigation(EmployeesScreen);
