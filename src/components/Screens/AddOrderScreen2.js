import React, {Component} from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather'
import {StyleSheet, Text,TouchableHighlight,TextInput, Image ,TouchableOpacity, ScrollView} from 'react-native';
import { Container, Header, Content, Form, Item, Picker,Body,Input,View,Left,Right,Button,Title} from 'native-base';

import { Avatar } from 'react-native-elements';

class AddOrderScreen2 extends React.Component {
  constructor(props) {
   super(props);
   this.state = {
     selected2: undefined,
      activeC: false, activeP:true
   };
 }
 onValueChange2(value: string) {
   this.setState({
     selected2: value
   });
 }

   _onHideUnderlay() {



   }
   _onShowUnderlay() {

    

   }

  render() {
    return (
       <Container>
       <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d'}}>
         <Left>
           <Button transparent onPress={() => this.props.navigation.goBack(null)}>
             <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
           </Button>
         </Left>
         <Body>

         </Body>
         <Right>
         <Button transparent onPress={() => this.props.navigation.navigate('Notification')}>
           <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications-outline" />
         </Button>
         <Button transparent onPress={() => this.props.navigation.navigate('Chat')}>
           <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
         </Button>
         </Right>
       </Header>

         <View style={styles.background}>
           <View style={styles.iconRing}>
             <View style={styles.image}>
             <Avatar
             imageProps={{resizeMode:'center'}}
             size="large"
             rounded
             source={require('./logocfm.png')}
             />
             </View>
             <View style={{flexWrap:'wrap'}}>
               <Text  style={styles.compNameStyle}>Company Name</Text><Text style={styles.workNumberStyle}>{"\n"}no work Orders</Text>
             </View>
           </View>
         </View>
         <View>
        <Text style={styles.quesTextStyle}>When?</Text>
         <Item picker style={{ borderBottomColor:'#F7F7F7',borderBottomWidth: 1,marginLeft:'5%'}}>
           <Picker
             mode="dropdown"
             iosIcon={<Ionicons name="ios-arrow-down" />}
             style={{ width: undefined,  borderBottomColor:'#F7F7F7',borderBottomWidth: 1, }}
             placeholder="Select your SIM"
             placeholderStyle={{ color: "#bfc6ea" }}
             placeholderIconColor="#007aff"
             selectedValue={this.state.selected2}
             onValueChange={this.onValueChange2.bind(this)}
           >
             <Picker.Item label="Select Category" value="key0" />
             <Picker.Item label="ATM Card" value="key1" />
             <Picker.Item label="Debit Card" value="key2" />
             <Picker.Item label="Credit Card" value="key3" />
             <Picker.Item label="Net Banking" value="key4" />
           </Picker>
            </Item>
            <Text style={styles.quesTextStyle}>Where?</Text>

           <TextInput placeholderTextColor={'#000'} style={styles.inputStyle}   placeholder="Floor Number"  style={styles.inputStyle}   />
           <TextInput placeholderTextColor={'#000'} style={styles.inputStyle}   placeholder="Office / Department"  style={styles.inputStyle}   />

           <TextInput placeholderTextColor={'#000'} style={styles.inputStyle}   placeholder="Full Address"  style={styles.inputStyle}   />
           <TextInput placeholderTextColor={'#000'} style={styles.inputStyle}   placeholder="Landmark"  style={styles.inputStyle}   />

       </View>

     <View style={{justifyContent:'space-around',flexDirection:'row',flexWrap:'wrap'}}>
       <TouchableHighlight style={styles.btnAccount }
         onHideUnderlay={this._onHideUnderlay.bind(this)}
         onShowUnderlay={this._onShowUnderlay.bind(this)}
         onPress={() => this.props.navigation.navigate('AddOrder')}
         underlayColor='#fff'>
           <Text style={ styles.textBtn} >Previous</Text>
         </TouchableHighlight>
        <TouchableHighlight style={ styles.btnAccount}
         onHideUnderlay={this._onHideUnderlay.bind(this)}
         onShowUnderlay={this._onShowUnderlay.bind(this)}
         onPress={()=>{}}
         underlayColor='#fff'>
           <Text style={styles.textBtn} >Submit</Text>
         </TouchableHighlight>

       </View>
       </Container>

  );
  }
  }
  const styles = StyleSheet.create({
    test:{
      flex:1,
    },
    container: {

      marginTop:'3%',
      display: 'flex',
      flex:1,


    },
    btnNext:{
      backgroundColor:'#e5322d',
      justifyContent:'center',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      width:'40%',
      marginTop:'5%',


    },
    btnAccount:{
      backgroundColor:'#e5322d',
      justifyContent:'center',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      width:'40%',
      marginTop:'3%',


    },
    btnAccountActive:{
      backgroundColor:'#fff',
      justifyContent:'center',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      width:'40%',
      marginTop:'3%',


    },
    btnAccount1:{
      backgroundColor:'#e5322d',
      justifyContent:'space-evenly',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      width:'45%',


    },
    btnAccountActive1:{
      backgroundColor:'#fff',
      justifyContent:'space-evenly',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      width:'45%',



    },
    textBtnActive:{
      paddingTop:'5%',
      paddingBottom:'10%',
      paddingLeft:'5%',
      justifyContent:'space-around',
      paddingRight:'5%',
      color:'#fff',
      width:'100%',
      textAlign:'center',
      fontSize:13,
      color:'#e5322d'
    },
    textBtn:{
      paddingTop:'5%',
      paddingBottom:'10%',
      paddingLeft:'5%',
      justifyContent:'space-around',
      paddingRight:'5%',
      color:'#fff',
      width:'100%',
      textAlign:'center',
      fontSize:13,
    },
    compNameStyle:{
      fontWeight:'bold',
      fontSize:18,
      color:'#fff',
      paddingLeft:'4%',
      paddingTop:'2%'
    },
    workNumberStyle:{

      fontSize:10,
      color:'#fff',
      paddingLeft:'11%',
    },
    quesTextStyle:{
      fontSize:16,
      color:'#e5322d',
      marginLeft:'5%',
      paddingTop:'4%',
    },
    image:{
      paddingBottom:'10%',
      paddingLeft:'5%',

    },
    iconRing:{
      flexDirection:'row',
      paddingLeft:'5%',

    },
    background:{
      width: '100%',
      backgroundColor: '#e5322d',
    },
    inputStyle:{
      borderBottomColor:'#F7F7F7',
      borderBottomWidth: 1,
      width:'100%',
      alignItems:'center',
      marginTop:0,
      marginLeft:'5%',
      paddingTop:0,
      fontSize:14,
    },
    inputStyle2:{
      borderBottomColor:'#F7F7F7',
      borderBottomWidth: 1,
      width:'45%',
      alignItems:'center',
      marginTop:0,
      paddingTop:0,
      fontSize:12,
    },
    submit:{
      flexDirection: 'row',
      justifyContent: 'center',

      marginTop:20,

  },
  submitText:{
      paddingTop:'3%',
      paddingBottom:'3%',
      textAlign:'center',
      color:'#fff',
      backgroundColor:'#e5322d',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      width:'45%',
      height:'100%',
      alignItems:'center',
      flexDirection: 'row',
      justifyContent: 'center',
    },

  })
  export default AddOrderScreen2;
