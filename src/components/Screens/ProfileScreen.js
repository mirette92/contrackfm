import React, {Component} from 'react';
import {Platform, StyleSheet,  View,Image} from 'react-native';
import { Container, Header, Content, Card, CardItem, Text, Body,Left,Right,Title,Button } from "native-base";
import { createStackNavigator, createAppContainer } from "react-navigation";

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Avatar } from 'react-native-elements';

class ProfileScreen extends React.Component {
  render() {
    return (
      <Container>
      <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d'}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.goBack(null)}>
            <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>My Profile</Title>
        </Body>
        <Right>
          <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications-outline" />
          </Button>
          <Button transparent onPress={() => this.props.navigation.navigate('Chat')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
          </Button>
        </Right>
      </Header>
        <Content style={{backgroundColor:'#E2E2E2'}}>

        <Card style={{backgroundColor:'#fff'}}>
        <CardItem>
          <Body >
            <View style={styles.CardProfile}>
            <View style={{flexDirection:'row'}}>
              <Avatar
              size="large"
              rounded
              source={{
                uri:
                  'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                }}
                showEditButton
                />
                <View style={{flexFlow:'column'||'wrap',paddingTop:'6%',paddingLeft:'5%'}}>
                <Text style={styles.title}>
                  YourName
                </Text>
                <Text style={styles.answers}>
                  Employee Name
                </Text>
                </View>
              </View>
              <View style={{flexFlow:'column'||'wrap',paddingTop:'6%'}}>

                <Text style={styles.title}>
                Email Address
                </Text>
                <Text style={styles.answers}>
                  ibrahim@bit68.com
                </Text>
                <Text style={styles.title}>
                  Mobile Number
                </Text>
                <Text style={styles.answers}>
                  012222222222
                </Text>
              </View>
            </View>

          </Body>
        </CardItem>
        </Card>

         <Card style={{backgroundColor:'#fff'}}>
         <CardItem>
           <Body >
             <Text>
                Office
             </Text>
             <Text style={styles.titleOffice}>
             <Ionicons name="ios-pin" style={{paddingRight:'1%'}}/>
                Nilecity ,cairo,egypt
             </Text>
             <Text style={styles.titleOffice}>
                <FontAwesome name="mobile-phone"  style={{paddingRight:'1%'}}/>
                Mobile :+0201002525
             </Text>
           </Body>
         </CardItem>
       </Card>
        </Content>

      </Container>

    );
  }
}

const styles = StyleSheet.create({
  CardProfile: {
    flexDirection:'column',

  },
  TextStyleVersion: {
    textAlign:'center',
  },
  title:{
    fontSize:12,color:'#c2c2c2',paddingTop:'5%'
  },
  titleOffice:{
    fontSize:15,color:'#c2c2c2',paddingTop:'2%'
  },
  answer:{
    fontSize:12,paddingLeft:'4%',color:'#252525',borderBottomWidth:1
  },
})
export default ProfileScreen;
