import React, {Component} from 'react';


import { Container, Header, Content, List, ListItem, Text, Icon, Right ,Left,Button,Body,Title} from 'native-base';
import { createStackNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';

import Feather from 'react-native-vector-icons/Feather'
import {StyleSheet} from 'react-native';

class NotificationScreen extends React.Component {



  render() {
    return (
      <Container >
        <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d'}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.goBack(null)}>
            <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
          </Button>
        </Left>
          <Body>
            <Title>Notification</Title>
          </Body>
          <Right>
          <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications-outline" />
          </Button>
          <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
          </Button>
          </Right>
        </Header>
        <Content style={{backgroundColor:'#E2E2E2'}}>
          <List>
            <ListItem noIndent  style={{backgroundColor:'#fff'}}>

              <Text style={styles.TextStyle}>Your techncian has arrived.{"\n"}work order #122345</Text>

            </ListItem>
            <ListItem noIndent  style={{backgroundColor:'#fff'}}>

              <Text style={styles.TextStyle}>Work Order #122345 has been authorized {"\n"} to be assigned to a techncian</Text>

            </ListItem>
            <ListItem noIndent  style={{backgroundColor:'#fff'}}>

              <Text style={styles.TextStyle}>Work Order #122345 has been authorized {"\n"} to be assigned to a techncian</Text>

            </ListItem>
            <ListItem noIndent  style={{backgroundColor:'#fff'}}>

              <Text style={styles.TextStyle}>Work Order #122345 has been authorized {"\n"} to be assigned to a techncian</Text>

            </ListItem>
            <ListItem noIndent  style={{backgroundColor:'#fff'}}>

              <Text style={styles.TextStyle}>Work Order #122345 has been authorized {"\n"} to be assigned to a techncian</Text>

            </ListItem>
            <ListItem noIndent  style={{backgroundColor:'#fff'}}>

              <Text style={styles.TextStyle}>Work Order #122345 has been authorized {"\n"} to be assigned to a techncian</Text>

            </ListItem>

          </List>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  TextStyle: {
    marginLeft:'3%'
  },
  TextStyleVersion: {
    textAlign:'center',
  },
})
export default NotificationScreen;
