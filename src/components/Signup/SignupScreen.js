//imports
import React, {Component} from 'react';
import {StyleSheet, View,TouchableHighlight,TextInput, Image ,TouchableOpacity,Text } from 'react-native';

import { Container, Header, Content, Tab, Tabs } from 'native-base';
import { createMaterialTopTabNavigator, createTabNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';

//body(jsx)
class SignupScreen extends Component{
  constructor(props) {
    super(props);
    this.state = { activeC: false, activeP:true};
    }
    _onHideUnderlay() {

        alert(this.state.activeC);

    }
    _onShowUnderlay() {

        alert('mirette');

    }



  render(){
    return (
    <View style={styles.container}>
          <View style={styles.container}>
            <TouchableHighlight style={
                    this.state.activeP
                        ? styles.btnAccount
                        : styles.btnAccountActive
                }
            onHideUnderlay={this._onHideUnderlay.bind(this)}
            onShowUnderlay={this._onShowUnderlay.bind(this)}
            onPress={()=>{}}
            underlayColor='#fff'>
              <Text style={
                        this.state.activeP
                            ? styles.textBtn
                            : styles.textBtnActive
                    } >Personal Account</Text>
            </TouchableHighlight>
            <TouchableHighlight style={
            this.state.activeC
            ? styles.btnAccount
            : styles.btnAccountActive
            }
            onHideUnderlay={this._onHideUnderlay.bind(this)}
            onShowUnderlay={this._onShowUnderlay.bind(this)}
            onPress={()=>{}}
            underlayColor='#fff'>
              <Text style={
                        this.state.activeC
                            ? styles.textBtn
                            : styles.textBtnActive
                    }>Corperate Account</Text>
            </TouchableHighlight>

          </View>


          <View  style={styles.marginTopView}>
            <View style={styles.paddingMarginInput}>
               <Text style={styles.StyleLabel}> Full Name</Text>
               <View style={styles.IconTextInput}>
                 <FontAwesome name="user" color="#e5322d" size={15} style={styles.marginTopIcon}/>
                 <TextInput  placeholderTextColor={'#000'} style={styles.inputStyle}   placeholder="Enter Full Name"/>
               </View>
            </View>
            <View style={styles.paddingMarginInput}>

               <Text style={styles.StyleLabel}>Email Address</Text>

              <View style={styles.IconTextInput}>
               <Ionicons name="ios-mail" color="#e5322d" size={15} style={styles.marginTopIcon}/>
               <TextInput  placeholderTextColor={'#000'} style={styles.inputStyle}   placeholder="Enter Email Address"/>
             </View>
            </View>
            <View style={styles.paddingMarginInput}>

                 <Text style={styles.StyleLabel}>Mobile</Text>

                <View style={styles.IconTextInput}>
                  <FontAwesome name="mobile" color="#e5322d" size={15} style={styles.marginTopIcon}/>
                  <TextInput  placeholderTextColor={'#000'} style={styles.inputStyle}   placeholder="Enter Mobile"/>
               </View>
            </View>
            <View style={styles.paddingMarginInput}>
              <Text style={styles.StyleLabel}>Password</Text>
              <View style={styles.IconTextInput}>
                <Ionicons name="ios-key" color="#e5322d" size={15} style={styles.marginTopIcon}/>
                <TextInput placeholderTextColor={'#000'} style={styles.inputStyle} secureTextEntry={true}  placeholder="Enter Password"/>
              </View>
            </View>
            <View style={styles.centerContent}>
              <TouchableHighlight style={styles.submit}
              onPress={() => this.props.navigation.navigate('Navbar')}
              underlayColor='#fff'>
              <Text style={styles.submitText}>Sign up </Text>
              </TouchableHighlight>
            </View>
          </View>

      </View>
    );

  }
}


class SignupScreenCoperate extends Component{
  render(){
    return (
      <View>
        <Text>Hello</Text>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  centerContent:{
    justifyContent:'center',
    alignItems:'center'
  },
  marginTopIcon:{
    marginTop:'2%',
    borderBottomWidth:0
  },
  IconTextInput:{
    flexDirection:'row',
    justifyContent:'space-evenly',
    borderBottomWidth:1,
    borderColor:'#ccc',
  },
  container: {
    marginTop:'3%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent:'space-around',
    alignItems:'center',
    flexWrap: 'wrap' ,

  },
  btnAccount:{
    backgroundColor:'#e5322d',
    justifyContent:'center',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#e5322d',
    width:'45%',


  },
  btnAccountActive:{
    backgroundColor:'#fff',
    justifyContent:'center',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#e5322d',
    width:'45%',



  },textBtnActive:{
    paddingTop:'5%',
    paddingBottom:'5%',
    paddingLeft:'5%',
    justifyContent:'center',
    paddingRight:'5%',
    color:'#fff',
    width:'100%',
    textAlign:'center',
    fontSize:13,
    color:'#e5322d'
  },
  textBtn:{
    paddingTop:'5%',
    paddingBottom:'5%',
    paddingLeft:'5%',
    justifyContent:'center',
    paddingRight:'5%',
    color:'#fff',
    width:'100%',
    textAlign:'center',
    fontSize:13,
  },
  BoxPersonal:{
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  StyleLabel:{
    marginLeft:'8.5%',
    marginBottom:0,
    paddingBottom:0,
    fontSize: 10,
    opacity: 0.3,
  },
  inputStyle:{
    borderBottomColor:'#F7F7F7',
    borderBottomWidth: 1.5,
    width:'90%',
    alignItems:'center',
    marginTop:0,
    paddingTop:0,
    fontSize:12,
  },
  ColorFontApp:{
    color: '#e5322d',
    fontWeight:'bold',
  },
  image: {
    height: null,
    width: '100%',
    },
    textStyle: {
      width: '100%',
        height: 50,
        backgroundColor: '#FF9800',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0
    },
    colorFont:{
      opacity: 0.3,
      paddingLeft:5,
    },
    background:{
      width: '100%',
      height: '40%',
      backgroundColor: '#e5322d',

    },
    marginTopView:{
      marginTop:'20%',
    },
    marginTopInput:{
      height:'3%',
    },
    paddingMarginInput:{
      paddingLeft:30,
      marginTop:10,

    },
    marginTopBtn:{
      marginTop:'10%',
      width:1,
      borderRadius:10,
      borderWidth: 1,

    },
    AccountBtn:{
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
    },
    submit:{
      flexDirection: 'row',
      justifyContent: 'center',
      marginTop:20,
      backgroundColor:'#e5322d',
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',

  },
  submitPersonal:{
    flex: 1,
    flexDirection: 'column',

    marginLeft:'1%',
    marginTop:20,
    width:'45%'

},
submitCoperate:{

  marginLeft:'1%',
  marginTop:20,
  width:'45%'

},
submitTextPersonal:{
    paddingTop:'3%',
    paddingBottom:'3%',
    textAlign:'center',
    color:'#fff',
    backgroundColor:'#e5322d',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#e5322d',


    alignItems:'center',
    flexDirection: 'row',
    justifyContent: 'center',
},
  submitText:{
      paddingTop:'3%',
      paddingBottom:'3%',
      textAlign:'center',
      color:'#fff',
      
      width:'90%',
      height:'100%',
      alignItems:'center',
      flexDirection: 'row',
      justifyContent: 'center',
  },
  textSignupStyle:{

    marginTop:'8%',
    alignItems: 'center',
    justifyContent: 'center',
  }

});




export default SignupScreen;

//export
