//imports
import React ,{Component} from 'react';

import {StyleSheet, Text, View,Button,TouchableHighlight,TextInput, Image ,TouchableOpacity,ScrollView} from 'react-native';


import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';



//body(class-[functions-methodes-jsx])
class SigninScreen extends Component{
  

  render(){
    return(
    <ScrollView keyboardShouldPersistTaps='handled' style={styles.container}>
        <View style={styles.background}>
          <Image style={styles.image} source={require('./icon1.png')}/>
        </View>
        <View style={styles.textStyle}>
          <Text style={styles.colorFont}>Sign in or Sign up to continue</Text>
        </View>
        <View  style={styles.marginTopView}>
          <View style={styles.paddingMarginInput}>
             <Text style={styles.StyleLabel}>Username</Text>
             <View style={styles.IconTextInput}>
               <FontAwesome name="user" color="#e5322d" size={15} style={{marginTop:'2%',}}/>
               <TextInput  placeholderTextColor={'#000'} style={styles.inputStyle}   placeholder="Enter Username" />
             </View>
          </View>
          <View style={styles.paddingMarginInput}>
            <Text style={styles.StyleLabel}>Password</Text>
            <View style={styles.IconTextInput}>
              <Ionicons name="ios-key" color="#e5322d" size={15} style={{marginTop:'2%',}}/>

              <TextInput placeholderTextColor={'#000'} style={styles.inputStyle} secureTextEntry={true}  placeholder="Enter Password"/>
            </View>
          </View>
          <TouchableHighlight style={styles.submit}
          onPress={() => this.props.navigation.navigate('Navbar')}
          underlayColor='#fff'>
          <Text style={styles.submitText}>Sign in</Text>
          </TouchableHighlight>
          <View style={styles.textSignupStyle}>
            <TouchableOpacity
                  style={styles.button}
                  onPress={() => this.props.navigation.navigate('Signup')} >
                  <Text style={styles.ColorFontApp}> Sign up </Text>
            </TouchableOpacity>
            <TouchableOpacity
                  style={styles.button}
                  onPress={() => this.props.navigation.navigate('ForgetPassword')} >
                  <Text style={styles.ColorFontApp}> | ForgetPassword </Text>
            </TouchableOpacity>
          </View>
        </View>
    </ScrollView>
    )
  }
}


//css (styling)
const styles = StyleSheet.create({
  container: {
    position:'absolute',
    top:0,
    bottom:0,
    left:0,
    right:0,
    display:'flex',
  },
  StyleLabel:{
    marginLeft:'8.5%',
    marginBottom:0,
    paddingBottom:0,
    fontSize: 10,
    opacity: 0.3,
  },
  inputStyle:{
    borderBottomColor:'#F7F7F7',
    borderBottomWidth: 1.5,
    width:'90%',
    alignItems:'center',
    marginTop:0,
    paddingTop:0,
    fontSize:12,
  },
  ColorFontApp:{
    color: '#e5322d',
    fontWeight:'bold',
  },
  image: {
    height: null,
    width: '100%',
    },
    textStyle: {
      marginTop:'8%',
      alignItems: 'center',
      justifyContent: 'center',
    },
    colorFont:{
      opacity: 0.3,
      paddingLeft:5,
    },
    background:{
      width: '100%',
      height: '100%',
      backgroundColor: '#e5322d',

    },
    marginTopView:{
      marginTop:'1%',
    },
    marginTopInput:{
      height:'3%',
    },
    IconTextInput:{
      flexDirection:'row',
      justifyContent:'space-evenly',
      borderBottomWidth:1,
      borderColor:'#ccc',
    },
    paddingMarginInput:{

      paddingLeft:30,
      marginTop:10,

    },

    submit:{
      flexDirection: 'row',
      justifyContent: 'center',
      marginRight:'4%',
      marginLeft:'1%',
      marginTop:20,
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#e5322d',
      backgroundColor:'#e5322d',
  },
  submitText:{
      paddingTop:'3%',
      paddingBottom:'3%',
      textAlign:'center',
      color:'#fff',
      width:'90%',
      height:'100%',
      alignItems:'center',
      flexDirection: 'row',
      justifyContent: 'center',
  },
  textSignupStyle:{

    marginTop:'8%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  }

});


//exports
export default SigninScreen;
